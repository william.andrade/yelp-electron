# yelp-electron #

First attempt at making a desktop application using Electron and React. Boilerplate for project foundation can be found [here] (https://github.com/electron/electron-quick-start).


Background Info and Usage
---
This is a simple application that just tries to recreate Yelp's most basic functionality. That is, to be able to get information for a business (primarily food) based on a search terms. I use 
[react-google-maps] (https://github.com/tomchentw/react-google-maps), a react based libary that implements Google's javascript map API, to display the first 50 businesses returned from Yelp based on the search terms entered. I also use [Material-UI] (http://www.material-ui.com/#/) components throughout various parts of the application. Using the app is as simple as entering a 'find' term (i.e. Tacos), and a 'search' term (i.e. Arlington, Va) to get results. I get data back directly from [Yelp's Fusion] (https://www.yelp.com/fusion)API.


Future Improvements/Edits
---
Disclaimer: I just started this project about a month ago! What's been implemented so far is basic functionality I thought of when I first began developing this project. Of course, along the way, I found ways to improve the app, but also things I wanted to completley get rid of. I summarize that stuff below:

* __Improvements__: 
    * __Redux__ - right now, the application uses nothing but internal state and props for its functionality. Currently at work, all I really use is Redux. I want to re-visit this project and add Redux to it. The funny thing about Redux is, it seems to make a small project more complex than it needs to be. But, it makes a large project a lot easier to manage. We'll see how this goes in the near future! 

    * __Google Map Improvments__ - I really like the react-google-maps pacakge I use to implement Google's map. Making a map into a useable React component was helpful for this project. Although, I want to add more map interaction/functionality. Things like resetting zoom boundaries, maybe even adding directions between points! 

    * __Yelp API Use__ - Yelp's Fusion API documentation is awesome, and it's free! Right now, I only really use their Search API for the core functionality of this app. But, they have other documentation for Reviews, Autocomplete suggestions, Transaction searches, etc. I would like to re-visit this app and make use of those other tools

    * __General Bug Fixes__ - I'm a one man QA team, and i'm sure this is riddled with "features". When I have enough free time in the evening, I try and re-visit this app to fix anything I might find wrong with it. All of that depends on my work week.


Setup/Install dependencies 
---

```
npm install
```
Usage
---

Start the dev server with this commmand:

```
npm start
```
