const React = require('react');
const {render} = require('react-dom');
require('./src/styles/main.scss');
require('leaflet/dist/leaflet.css');

import App from './src/app';
import injectTapEventPlugin from 'react-tap-event-plugin';


const containerEl = document.getElementById('app');
injectTapEventPlugin();

render(
  <App />,
  containerEl
);