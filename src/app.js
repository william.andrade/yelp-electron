import React, {Component} from 'react';

import GoogleMaps from './components/GoogleMaps';


export default class App extends Component {

  render() {
    return (
      <div id="app">
        <GoogleMaps/>
      </div>
    );
  }
}