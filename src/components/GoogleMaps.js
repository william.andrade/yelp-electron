/* global google */
import React, {Component} from 'react';
import {withGoogleMap, GoogleMap, Marker} from "react-google-maps";
import MarkerClusterer from "react-google-maps/lib/addons/MarkerClusterer";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import GooglePanel from './GooglePanel';
import YelpDialog from './YelpDialog';

const DEFAULT_VIEWPORT = {
  center: {lat: 38.89598, lng: -77.03673},
  zoom: 12,
  atlCenter: {lat: 33.8777138770643, lng: -84.4078759}
};

const MyGoogleMap = withGoogleMap(props => (
  <GoogleMap
    zoom={props.zoom}
    maxZoom={15}
    center={props.submitPressed ? {lat: props.center.latitude, lng: props.center.longitude} : props.center}
    controlPosition={google.maps.ControlPosition.TOP_CENTER}>

    <MarkerClusterer averageCenter enableRetinaIcons gridSize={60}>
      {props.markers.map((place, index) =>
        <Marker
          key={`marker-${index}`}
          position={{lat: place.coordinates.latitude, lng: place.coordinates.longitude}}
          animation={google.maps.Animation.DROP}
          onClick={() => props.onMarkerClick(place)}
        />
      )}
    </MarkerClusterer>
  </GoogleMap>
));


class GoogleMaps extends Component {
  constructor(props) {
    super(props);

    this.state = {
      googleMarkers: [],
      yelpBusiness: {},
      submitPressed: false,
      resetPressed: false,
      center: DEFAULT_VIEWPORT.center,
      open: false
    };

    this.handlePanelSubmit = this.handlePanelSubmit.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.handleNavClicked = this.handleNavClicked.bind(this);
    this.handleMarkerClick = this.handleMarkerClick.bind(this);
    this.handleDialogClose = this.handleDialogClose.bind(this);
  }

  handlePanelSubmit(data) {
    this.setState({
      googleMarkers: data.businesses,
      center: data.region.center,
      submitPressed: true
    });
  };

  handleReset(pressed) {
    if(pressed){
      this.setState({
        googleMarkers: [],
        yelpBusiness: {},
        submitPressed: false,
        resetPressed: true,
        center: DEFAULT_VIEWPORT.center,
        open: false
      });
    }
  }


  handleNavClicked(singleBusiness) {
    this.setState({
      yelpBusiness: singleBusiness,
      center: singleBusiness.coordinates
    });
  }

  handleMarkerClick(yelpBusiness) {
    this.setState({
      open: true,
      yelpBusiness: yelpBusiness,
      center: yelpBusiness.coordinates
    })
  }

  handleDialogClose() {
    this.setState({open: false});
  }

  render() {
    const {googleMarkers, center, submitPressed, open, yelpBusiness} = this.state;

    return (
      <MuiThemeProvider>
        <div style={{overflow: 'hidden'}}>

          <GooglePanel
            onSubmit={this.handlePanelSubmit}
            onNav={this.handleNavClicked}
            onReset={this.handleReset}
          />

          <YelpDialog
            open={open}
            onDialogClose={this.handleDialogClose}
            yelpBusiness={yelpBusiness}
          />

          <div id="google-map-canvas">
            <MyGoogleMap
              containerElement={<div style={{ height: `inherit`, width: `inherit` }} />}
              mapElement={<div style={{ height: `100%`, width: `100%`, position: `absolute`, top: `0px`, left: `0px`, backgroundColor: `rgb(229, 227, 223)`}} />}
              markers={googleMarkers}
              center={center}
              zoom={DEFAULT_VIEWPORT.zoom}
              submitPressed={submitPressed}
              onMarkerClick={this.handleMarkerClick}
            />
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}
export default GoogleMaps;