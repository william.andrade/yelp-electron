/* global google */
import 'whatwg-fetch';
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import RaisedButton from 'material-ui/RaisedButton';
import SearchIcon from 'material-ui/svg-icons/action/search';
import RestorePage from 'material-ui/svg-icons/action/settings-backup-restore';
import yelpImg from '../img/Yelp_trademark_RGB_outline.png';
import Autocomplete from 'react-google-autocomplete';
import YelpList from './YelpList';
import {yelpConfig} from "../appConfig";
import {isEmptyOrWhitespace} from "../util";

const search_limit = 50;
const EMPTY_STRING = '';

const INPUT_STYLE = {
  height: 30,
  display: 'block',
  outline: 'none',
  margin: 0,
  width: '275px',
  fontFamily: 'sans-serif',
  fontSize: 15,
  appearance: 'none',
  boxShadow: 'none',
  borderRadius: 5
};

const footerStyles = {
  root: {
    backgroundColor: 'rgb(211, 35, 35)',
    height: '24%',
    display: 'flex',
    justifyContent: 'space-around'
  },
  gitlabLink: {
    textDecoration: 'none',
    color: '#fff',
    fontSize: 25,
    marginTop: 7
  },
  emailLink: {
    textDecoration: 'none',
    color: '#fff',
    fontSize: 25,
    marginTop: 2
  }
};


class GooglePanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      findTerm: EMPTY_STRING,
      nearTerm: EMPTY_STRING,
      searchInitiated: false
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.onReset = this.onReset.bind(this);
    this.onFindSelected = this.onFindSelected.bind(this);
    this.onNearSelected = this.onNearSelected.bind(this);

    this.onFindTyped = this.onFindTyped.bind(this);
    this.onNearTyped = this.onNearTyped.bind(this);
    this.handleNavSelected = this.handleNavSelected.bind(this);
  }

  handleNavSelected(yelpItem) {
    this.props.onNav(yelpItem);
  }

  onFindTyped(value) {
    this.setState({findTerm: value});
  }

  onNearTyped(value) {
    this.setState({nearTerm: value});
  }

  onFindSelected(find) {
    this.setState({findTerm: find.name});
  }

  onNearSelected(near) {
    this.setState({nearTerm: near.formatted_address});
  }

  onReset() {
    this.setState({
      data: [],
      findTerm: EMPTY_STRING,
      nearTerm: EMPTY_STRING,
      searchInitiated: false
    });

    this.props.onReset(true);
  }


  onSubmit() {
    const {findTerm, nearTerm} = this.state;

    console.log('FIND: ', findTerm);
    console.log('NEAR: ', nearTerm);

    const options = {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json',
        'Authorization': `${yelpConfig.token_type} ${yelpConfig.access_token}`
      },
      credentials: 'same-origin'
    };

    //const radius = 8046;
    //`${request_url}?term=${findTerm}&location=${nearTerm}&radius=${radius}`

    fetch(`${yelpConfig.request_url}?term=${findTerm}&location=${nearTerm}&limit=${search_limit}`, options)
      .then((response) => {
        return response.json();
      })
      .then((json) => {
        console.log('JSON: ', json);
        this.setState({data: json, searchInitiated: true});
        this.props.onSubmit(json);
      }).catch((error) => {
      console.log('error: ', error)
    });
  }

  render() {
    const {findTerm, nearTerm} = this.state;
    const emptySearchTerms = isEmptyOrWhitespace(findTerm) || isEmptyOrWhitespace(nearTerm);


    return (
      <div className="google-side-panel">

        <header id="yelp-header">
          <img id="yelp-logo" src={yelpImg}/>
        </header>

        <form id="yelp-form">
          <div className="input-container">
            <label>Find</label>
            <Autocomplete
              type="text"
              onChange={(e) => {this.onFindTyped(e.target.value);}}
              style={INPUT_STYLE}
              value={this.state.findTerm}
              types={['establishment', 'food']}
              componentRestrictions={{country: 'us'}}
              placeholder="restaurants, bars, delivery  "/>
          </div>

          <div className="input-container">
            <label>Near</label>
            <Autocomplete
              type="text"
              onPlaceSelected={(place) => {this.onNearSelected(place)}}
              onChange={(e) => {this.onNearTyped(e.target.value);}}
              style={INPUT_STYLE}
              value={this.state.nearTerm}
              types={['geocode']}
              componentRestrictions={{country: 'us'}}
              placeholder="address, neighborhood, city, state"/>
          </div>


          <div className="buttons-container" style={{display: 'flex', flexDirection: 'row'}}>
            <RaisedButton
              className="reset"
              label="Reset"
              disabled={emptySearchTerms ? true : false}
              style={{marginLeft: 30}}
              backgroundColor={'rgb(211, 35, 35)'}
              labelColor={'rgb(255, 255, 255)'}
              icon={<RestorePage/>}
              onTouchTap={this.onReset}
            />

            <RaisedButton
              className="search"
              label="Search"
              style={{marginLeft: 15}}
              backgroundColor={'rgb(211, 35, 35)'}
              labelColor={'rgb(255, 255, 255)'}
              icon={<SearchIcon/>}
              onTouchTap={this.onSubmit}/>
          </div>

        </form>

        <YelpList
          yelpData={this.state.data.businesses}
          searched={this.state.searchInitiated}
          onNavSelected={this.handleNavSelected}
        />

        {/*
         <footer className="yelp-footer" style={footerStyles.root}>
         <a className="fa fa-lg fa-gitlab fa-2x" style={footerStyles.gitlabLink} href={"https://gitlab.com/william.andrade/yelp-electron"} target="_blank"/>
         <a className="fa fa-envelope-o" style={footerStyles.emailLink} href={"mailto:andrade.william61@gmail.com"} />
         </footer>
         */}

      </div>
    );
  }
}

GooglePanel.propTypes = {
  onSubmit: PropTypes.func,
  onNav: PropTypes.func,
  onReset: PropTypes.func
};

GooglePanel.defaultpropTypes = {
  onSubmit: () => {},
  onNav: () => {},
  onReset: () => {}
};

export default GooglePanel;