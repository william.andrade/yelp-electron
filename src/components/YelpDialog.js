import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Divider from 'material-ui/Divider';
import {GridTile} from 'material-ui/GridList';
import {lightBlue700} from 'material-ui/styles/colors'
import {getStarRatings} from '../util';

const styles = {
  flexColumn: {
    display: 'flex',
    flexDirection: 'column'
  },
  yelpDialogBody: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginTop: 10
  },
  yelpInfo: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
    marginLeft: 10,
    marginTop: 10
  },
  yelpLink: {
    textDecoration: 'none',
    color: lightBlue700
  }
};


class YelpDialog extends PureComponent {
  constructor(props) {
    super(props);
    this.getCategories = this.getCategories.bind(this);
    this.getAddress = this.getAddress.bind(this);
    this.getTransactions = this.getTransactions.bind(this);
  }

  getCategories(categories) {
    let categoriesString = '';
    let titleArray = [];
    if (categories === undefined || !categories.length) { //if our business has no category info
      return '';
    } else {
      categories.forEach(function (categoryItem) {
        titleArray.push(categoryItem.title);
      });
      categoriesString = titleArray.join(', ');
    }
    return categoriesString;
  }

  getAddress(location) {
    let address;
    if (location === undefined || !location.display_address.length) {
      return '';
    } else {
      address = (
        <div style={{...styles.flexColumn, marginLeft: 10, marginTop: 10}}>
          <span>{location.display_address[0]}</span>
          <span>{location.display_address[1]}</span>
          <span>{location.display_address.length > 2 ? location.display_address[2] : ''}</span>
        </div>
      );
    }
    return address;
  }

  getTransactions(transactions) {
    let transactionString = '';
    if (transactions === undefined || !transactions.length) {
      return 'Transactions: no information';
    } else {
      transactionString = transactions.join(', ');
    }
    return 'Transactions: ' + transactionString;
  }

  render() {
    const {onDialogClose, yelpBusiness} = this.props;
    let starRating = getStarRatings(yelpBusiness.rating);
    let categories = this.getCategories(yelpBusiness.categories);
    let businessAddress = this.getAddress(yelpBusiness.location);
    let businessTransactions = this.getTransactions(yelpBusiness.transactions);

    const actions = [
      <FlatButton
        label="Close"
        onClick={onDialogClose}
      />
    ];

    return (
      <Dialog
        className="yelp-dialog"
        open={this.props.open}
        modal={false}
        actions={actions}
        title={<h3><a style={styles.yelpLink} className="yelp-link" target="_blank" href={yelpBusiness.url}>{yelpBusiness.name}</a></h3>}
      >
        <Divider className="dialog-divider" style={{height: 2}}/>

        <div style={styles.yelpDialogBody}>
          <GridTile className="yelp-pic" style={{height: 205, width: 205}}>
            <img src={yelpBusiness.image_url}/>
          </GridTile>

          <div style={styles.flexColumn}>
            <div id="star-rating-container">
              {starRating}
              <div style={{marginLeft: 10}}>{`${yelpBusiness.review_count} reviews`}</div>
            </div>

            <div style={styles.yelpInfo}>
              <span>{yelpBusiness.price && yelpBusiness.price !== '' ? yelpBusiness.price : '$'}</span>
              <span style={{marginLeft: 8, marginRight: 8}}> • </span>
              <span>{categories}</span>
            </div>

            <div style={styles.yelpInfo}>
              <span>{`Phone: ${yelpBusiness.display_phone}`}</span>
            </div>

            <div style={styles.yelpInfo}>
              <span>{yelpBusiness.is_closed === true ? 'Open: No' : 'Open: Yes'}</span>
            </div>

            {businessAddress}

            <div style={styles.yelpInfo}>
              {businessTransactions}
            </div>

          </div>
        </div>
      </Dialog>
    );
  }
}

YelpDialog.propTypes = {
  open: PropTypes.bool,
  onDialogClose: PropTypes.func,
  yelpBusiness: PropTypes.object
};

YelpDialog.defaultProps = {
  open: false,
  onDialogClose: () => {},
  yelpBusiness: {}
};

export default YelpDialog;
