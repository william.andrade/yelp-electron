import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {GridList, GridTile} from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import Subheader from 'material-ui/Subheader';
import InfoOutline from 'material-ui/svg-icons/action/info-outline';
import fourStar from '../img/yelp_stars/web/small/small_4@2x.png';


const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around'
  },
  gridList: {
    width: 500,
    height: 490,
    overflowY: 'auto'
  }
};


class YelpGridList extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    const {yelpData} = this.props;
    console.log('YELP_DATA: ', yelpData);
    
    return (
      <div style={styles.root} className="yelp-grid-list">
        <GridList style={styles.gridList} cols={1} cellHeight={180} padding={4}>
          <Subheader>Yelp List</Subheader>
          {yelpData.map((yelpItem, index) => (
            <GridTile
              key={index}
              title={yelpItem.name}
              actionIcon={<IconButton tooltip="Additional information" tooltipPosition="top-left"><InfoOutline color="white" /></IconButton>}
              subtitle={<span><b>{`${yelpItem.review_count} reviews`}</b></span>}
              //subtitle={yelpItem.rating === 4 ? <img src={fourStar}/> : null}
              titleStyle={{fontFamily: 'Roboto, sans-serif', fontSize: 16}}
              titleBackground="linear-gradient(to top, rgba(0,0,0,0.7) 0%,rgba(0,0,0,0.3) 70%,rgba(0,0,0,0) 100%)"
              subtitleStyle={{fontFamily: 'Roboto, sans-serif'}}
            >
              <img src={yelpItem.image_url}/>
            </GridTile>
          ))}
        </GridList>
      </div>
    );
  }
}


YelpGridList.propTypes = {
  yelpData: PropTypes.array
};

YelpGridList.defaultProps = {
  yelpData: []
};


export default YelpGridList;
