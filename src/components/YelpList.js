import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {List, ListItem} from 'material-ui/List';
import Paper from 'material-ui/Paper';
import Divider from 'material-ui/Divider';
import Avatar from 'material-ui/Avatar';
import IconButton from 'material-ui/IconButton';
import NearMe from 'material-ui/svg-icons/maps/near-me';
import {lightBlue700} from 'material-ui/styles/colors'

const styles = {
  listRoot: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    backgroundColor: '#fff'
  },
  list: {
    width: '100%',
    overflowY: 'auto',
    padding: 0
  },
  yelpPaper: {
    height: 514
  },
  messageStyle: {
    textAlign: 'center',
    verticalAlign: 'middle',
    lineHeight: '250px',
    background: '#f5f5f5',
    borderBottom: '1px solid #e6e6e6',
    fontSize: 21
  }
};

class YelpList extends PureComponent {
  constructor(props) {
    super(props);
    this.onNavClick = this.onNavClick.bind(this);
  }
  onNavClick(yelpItem) {
    this.props.onNavSelected(yelpItem);
  }

  render() {
    const {yelpData, searched} = this.props;

    if (searched && yelpData.length) { //a search was initiated and returned results
      return (
        <div style={styles.listRoot}>
          <List style={styles.list} className="yelp-list">
            {yelpData.map((yelpListItem, index) => (
              <div key={`yelp-item-${index}`}>
                <ListItem
                  style={{cursor: 'default'}}
                  hoverColor="none"
                  className="yelp-list-item"
                  disableTouchRipple={true}
                  leftAvatar={<Avatar src={yelpListItem.image_url}/>}
                  primaryText={yelpListItem.name}
                  secondaryText={<span><b>{`${yelpListItem.location.city}, ${yelpListItem.location.state}`}</b></span>}
                  rightIconButton={
                  <IconButton
                  className="yelp-button"
                  onClick={ ()=> this.onNavClick(yelpListItem, index)}
                  iconStyle={{color: lightBlue700}}
                  tooltip="Navigate on map"
                  tooltipPosition="bottom-left"
                  //disabled={true}
                  >
                  <NearMe className="yelp-icon"/>
                  </IconButton>
                  }
                />
                <Divider/>
              </div>
            ))}
          </List>
        </div>
      );
    } else if (searched && !yelpData.length) { //a search was initiated and returned NO results
      return (
        <div>
          <Paper className="yelp-paper-one" style={styles.yelpPaper}>
            <div style={styles.messageStyle}>
              Sorry! no results were found, try again
            </div>
          </Paper>
        </div>
      );
    } else { //default welcome dialog
      return (
        <div>
          <Paper className="yelp-paper-two" style={styles.yelpPaper}>
            <div style={styles.messageStyle}>
              Welcome!
            </div>
          </Paper>
        </div>
      );
    }
  }
}

YelpList.propTypes = {
  yelpData: PropTypes.array,
  searched: PropTypes.bool,
  onNavSelected: PropTypes.func
};

YelpList.defaultProps = {
  yelpData: [],
  searched: false,
  onNavSelected: () => {}
};


export default YelpList;
