import React, {Component} from 'react';
import {Map, Marker, Popup, TileLayer, ZoomControl} from 'react-leaflet';
import YelpPanel from './YelpPanel';

const position = [38.89598, -77.03673];

const DEFAULT_VIEWPORT = {
  center: [38.89598, -77.03673],
  zoom: 12
};


class YelpMap extends Component {

  constructor(props) {
    super(props);

    this.state = {
      viewport: DEFAULT_VIEWPORT,
      yelpMarkers: [],
      isInitialized: false
    };

    this.onClickReset = this.onClickReset.bind(this);
    this.onViewportChanged = this.onViewportChanged.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

  }

  onClickReset() {
    this.setState({viewport: DEFAULT_VIEWPORT});
  }

  onViewportChanged(viewport) {
    this.setState({viewport})
  }


  handleSubmit(data) {
    let viewPort = {};
    if (data) {
      viewPort.center = [data.region.center.latitude, data.region.center.longitude];
      viewPort.zoom = 12;
    }
    this.setState({yelpMarkers: data.businesses, viewPort: viewPort, isInitialized: true});
  }


  render() {

    const {yelpMarkers, viewport, isInitialized} = this.state;
    let initialized = isInitialized ? true : false;

    return (
      <div id="mapUI">
        <YelpPanel onSubmit={this.handleSubmit}/>

        <Map
          center={initialized ? viewport.center : DEFAULT_VIEWPORT.center}
          zoom={initialized ? viewport.zoom : DEFAULT_VIEWPORT.zoom}
          zoomControl={false}
          onViewportChanged={this.onViewportChanged}
          viewport={viewport}>

          <TileLayer
            url='https://api.mapbox.com/styles/v1/mapbox/traffic-day-v2/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoid2lsbGlhbWFuZHJhZGUiLCJhIjoiY2o2OG9xMHIyMGpyeTM3bnlndmt5dDU1dSJ9.7sggpTuzSDmC61AZ2QoLnQ'
            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          />
          <ZoomControl position="bottomright"/>

          {yelpMarkers.map((place, index) =>
            <Marker key={`marker-${index}`} position={[place.coordinates.latitude, place.coordinates.longitude]}>
              <Popup>
                <span>{place.location.address1}</span>
              </Popup>
            </Marker>
          )}
        </Map>
      </div>

    );
  }

}


export default YelpMap;