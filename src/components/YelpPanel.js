import React, {Component} from 'react';
import PropTypes from 'prop-types';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import SearchIcon from 'material-ui/svg-icons/action/search';
import 'whatwg-fetch';


const request_url = 'https://api.yelp.com/v3/businesses/search';
const searchRequest = {
  term: 'Surfside',
  location: '3832 calvert street NW washington dc'
};

const yelp_auth_info = {
  access_token: "3B2-bxfSCwwlaFYlBxJpG86tHo9Ulsr1gN39AnmXfsyLMHeZcWKAVLNsyslfVNgFCNsTTDiXRyj3V-WtLLbMiINKsvendBb1lqBnVIasVCdzZjcerMpFTqGF342CWXYx",
  expires_in: 15551999,
  token_type: "Bearer"
};

const styles = {
  textFloatingLabel : {
    color: '#666',
    fontSize: 20
  },
  textUnderlineFocus : {
    borderColor: 'rgb(211, 35, 35)'
  }
};



class YelpPanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    };

    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    console.log('YELP_PANEL');
  }

  onSubmit() {

    const options = {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + yelp_auth_info.access_token
      },
      //body: searchRequest,
      //mode: 'no-cors',
      credentials: 'same-origin'
    };

    fetch(`${request_url}?term=${searchRequest.term}&location=${searchRequest.location}`, options)
      .then((response) => {
        return response.json();
      })
      .then((json) => {
        console.log('JSON: ', json);
        this.setState({data: json});
        this.props.onSubmit(json);
      }).catch((error)=> {
      console.log('error: ', error)
    });

  }


  render() {

    return (
      <MuiThemeProvider>
        <div className="yelp-panel">

          <TextField
            hintText= "tacos, asian, indian"
            hintStyle={{fontSize: 15}}
            floatingLabelStyle={styles.textFloatingLabel}
            underlineFocusStyle={styles.textUnderlineFocus}
            floatingLabelText="Find"
          />

           <TextField
            hintText= "address, neighborhood, city or state"
            hintStyle={{fontSize: 15}}
            floatingLabelStyle={styles.textFloatingLabel}
            underlineFocusStyle={styles.textUnderlineFocus}
            floatingLabelText="Near"
           />

          <RaisedButton
            label="Search"
            style={{marginLeft: 30}}
            backgroundColor={'rgb(211, 35, 35)'}
            labelColor={'rgb(255, 255, 255)'}
            icon={<SearchIcon/>}
            onTouchTap={this.onSubmit}/>

        </div>
      </MuiThemeProvider>
    );
  }
}


YelpPanel.propTypes = {
  onSubmit: PropTypes.func

};

YelpPanel.defaultpropTypes = {
  onSubmit: () => {}
};

export default YelpPanel;