import oneStars from './img/yelp_stars/web/small/small_1@2x.png';
import oneHalfStars from './img/yelp_stars/web/small/small_1_half@2x.png'
import twoStars from './img/yelp_stars/web/small/small_2@2x.png';
import twoHalfStars from './img/yelp_stars/web/small/small_2_half@2x.png';
import threeStars from './img/yelp_stars/web/small/small_3@2x.png';
import threeHalfStars from './img/yelp_stars/web/small/small_3_half@2x.png';
import fourStars from './img/yelp_stars/web/small/small_4@2x.png';
import fourHalfStars from './img/yelp_stars/web/small/small_4_half@2x.png'
import fiveStars from './img/yelp_stars/web/small/small_5@2x.png';
import noStars from './img/yelp_stars/web/small/small_0@2x.png';
import React from 'react';

export function getStarRatings(rating){
  let ratingImg;
  if(rating % 1 != 0) {
    switch (rating) {
      case 1.5:
        ratingImg = (<img src={oneHalfStars}/>);
        break;
      case 2.5:
        ratingImg = (<img src={twoHalfStars}/>);
        break;
      case 3.5:
        ratingImg = (<img src={threeHalfStars}/>);
        break;
      case 4.5:
        ratingImg = (<img src={fourHalfStars}/>);
        break;
      case 5.5:
        ratingImg = (<img src={fiveStars}/>);
        break;
      default:
        ratingImg = (<img src={noStars}/>);
    }
    return ratingImg;
  }
  else {
    switch (rating) {
       case 1:
        ratingImg = (<img src={oneStars}/>);
        break;
      case 2:
        ratingImg = (<img src={twoStars}/>);
        break;
      case 3:
        ratingImg = (<img src={threeStars}/>);
        break;
      case 4:
        ratingImg = (<img src={fourStars}/>);
        break;
      case 5:
        ratingImg = (<img src={fiveStars}/>);
        break;
      default:
        ratingImg = (<img src={noStars}/>);
    }
    return ratingImg;
    }
}

export const isEmptyOrWhitespace = (str) => {
  if (str === undefined || str === null) {
    return true;
  }

  if (str.length > 0) {
    for (let i = 0; i < str.length; i++) {
      if (str[i] !== ' ') {
        return false;
      }
    }
  }

  return true;
};
